# python-in-mswindow
When your work has ms excel heavily used, you will have to use aa ms windows machine.
My stack is python and working with Python from a ms-windows 10 machine is something very UGLY - yes I have to use the word.

An easier way maybe using PyCharm from the start.


# my struggle in windows10 note
Here I apply my habit from ubuntu desktop - two critical tools when working with Python in my view is **pyenv** and **pipenv**
A google search on the topic **pyenv in windows 10** will lead you to [this guide](https://github.com/pyenv-win/pyenv-win#installation)

though I cannot get it working on my windows 10 laptop

try with windows command line
```
: .\python-in-mswindow>
pyenv --version
    error="
    'pyenv' is not recognized as an internal or external command,
    operable program or batch file.
    "
```

try with windows terminal - what the h*ck is the multi-name confusion
```
PS C:\Users\namgivu> 
pyenv versions
    shoulde_see="
      3.6.8
      3.7.6
    * 3.8.1 (set by C:\Users\namgivu\.pyenv\pyenv-win\version)
    "
```
by windows terminal we mean [this](./windows-terminal.jpg)

view it as snapshot on my mswindows10 [here](./pyenv-notalwaysworking-mswindows10.png)

# what I tried and failed
```bash
git clone https://github.com/pyenv-win/pyenv-win.git "%USERPROFILE%/.pyenv"
```
